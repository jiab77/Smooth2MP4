# Smooth2MP4
Just a POC to convert old VC1 SmoothStreaming files to MP4

**Required softwares:**
- [FFMpeg](https://www.ffmpeg.org)
- [MP4Box](https://gpac.wp.mines-telecom.fr/mp4box/)
- [MP4Split](http://docs.unified-streaming.com/installation/mp4split.html) (*You may require a license to use MP4Split, [get a license key](http://docs.unified-streaming.com/installation/license.html)*)